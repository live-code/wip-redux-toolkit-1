import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../model/product';
import { setError } from '../../../core/store/http-error.store';

export const productsStore = createSlice({
  name: 'products',
  initialState: [] as Product[],
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      return [...action.payload];
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.push(action.payload);
    },
    toggleProductSuccess(state, action: PayloadAction<Product>) {
      const product = state.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = action.payload.visibility;
      }
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      const index = state.findIndex(p => p.id === action.payload)
      state.splice(index, 1)
    }
  }
})
export const {
  addProductSuccess, getProductsSuccess, toggleProductSuccess, deleteProductSuccess
} = productsStore.actions;
