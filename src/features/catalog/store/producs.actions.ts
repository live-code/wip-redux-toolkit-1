import { AppThunk } from '../../../App';
import Axios from 'axios';
import { getProductsSuccess, deleteProductSuccess, addProductSuccess, toggleProductSuccess } from './products.store';
import { Product } from '../model/product';
import { setError } from '../../../core/store/http-error.store';
import { createAsyncThunk } from '@reduxjs/toolkit';

export const getProducts = (): AppThunk => async (dispatch) => {
  dispatch({ type: 'getProducts'})
  try {
    const response = await Axios.get<Product[]>('http://localhost:3001/products')
    dispatch(getProductsSuccess(response.data))
    dispatch(setError(false));
  } catch(err) {
    dispatch(setError(true));
  }
}


export const deleteProduct = (id: number): AppThunk => async dispatch => {
  dispatch({ type: 'deleteProduct', payload: id})
  try {
    await Axios.delete('http://localhost:3001/products/' + id)
    dispatch(deleteProductSuccess(id))
  } catch(err) {}
}

export const addProduct = createAsyncThunk<
  Product,
  Pick<Product, 'title' | 'price'>
>(
  'products/add',
  async (payload, { dispatch, rejectWithValue }) => {
    const newProduct: Omit<Product, 'id'> = { ...payload, visibility: false };
    try {
      const response = await Axios.post<Product>('http://localhost:3001/products/', newProduct)
      dispatch(addProductSuccess(response.data))
      return response.data;
    } catch(err) {
      console.log('errore')
      return rejectWithValue('erorre vario!')
    }
  }
)

/*
export const addProduct = (
  product: Omit<Product, 'id' | 'visibility' >
): AppThunk => async (dispatch) => {
  dispatch({ type: 'products/addProduct', payload: product});

  const newProduct: Omit<Product, 'id'> = { ...product, visibility: false };
  try {
    const response = await Axios.post('http://localhost:3001/products/', newProduct)
    dispatch(addProductSuccess(response.data))
  } catch(err) {}
}
*/

export const toggleProduct = (product: Product): AppThunk => async dispatch => {
  try {
    const updatedProduct: Product = {
      ...product,
      visibility: !product.visibility
    };
    const response = await Axios.patch<Product>(`http://localhost:3001/products/${product.id}`, updatedProduct);
    dispatch(toggleProductSuccess(response.data))
  } catch (err) {
    // dispatch error
  }
};











