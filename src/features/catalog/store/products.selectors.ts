import { RootState } from '../../../App';
import { Product } from '../model/product';

export const selectProductsList = (state: RootState) => state.catalog;
export const selectProductsTotal = (state: RootState) => state.catalog.reduce((acc: number, curr: Product) => {
  return acc + curr.price;
}, 0)
