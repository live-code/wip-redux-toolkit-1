import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addProduct, deleteProduct, getProducts, toggleProduct } from './store/producs.actions';
import { selectProductsList, selectProductsTotal } from './store/products.selectors';
import { ProductsList } from './components/ProductsList';
import { ProductForm } from './components/ProductForm';
import { selectHttpError } from '../../core/store/http-error.selector';
import { AppDispatch } from '../../App';

export const CatalogPage: React.FC = () => {
  const dispatch = useDispatch() as AppDispatch;
  const products = useSelector(selectProductsList);
  const total = useSelector(selectProductsTotal);
  const error = useSelector(selectHttpError);
  // const toast = useToast();
  useEffect(() => {
    dispatch(getProducts())
  }, [dispatch])

  const addHandler = (title: string) => {
    dispatch(addProduct({ title, price: 10}))
      .then((res: any) => console.log('done', res))
  }
  return <div>
    { error && <div className="alert alert-danger">Errore server side</div>}
    <ProductForm onSubmit={addHandler} />

    <ProductsList
      total={total}
      products={products}
      toggle={p => dispatch(toggleProduct(p))}
      delete={(id) => dispatch(deleteProduct(id))}
    />

  </div>
};
