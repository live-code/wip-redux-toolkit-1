import React  from 'react';
import { deleteProduct, toggleProduct } from '../store/producs.actions';
import { Product } from '../model/product';

interface ProductsListProps {
  total: number;
  products: Product[];
  toggle: (product: Product) => void
  delete: (productId: number) => void
}

export const ProductsList: React.FC<ProductsListProps> = (props) => {

  function toggleHandler(p: Product, e: React.MouseEvent<HTMLSpanElement>) {
    e.stopPropagation();
    props.toggle(p);
  }

  return <div>
    {
      props.products.map(p => {
        return <li
          key={p.id}
          onClick={() => props.delete(p.id)}
        >
          <span onClick={(e) => toggleHandler(p, e)}>
            {
              p.visibility ?
                <i className="fa fa-eye" /> :
                <i className="fa fa-eye-slash" />
            }
          </span>
          {p.title}

        </li>
      })
    }
    total: {props.total}
    </div>
};
