import React  from 'react';
import { addProduct } from '../store/producs.actions';

interface ProductFormProps {
  onSubmit: (productName: string) => void;
}
export const ProductForm: React.FC<ProductFormProps> = (props) => {
  function addTodoHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      props.onSubmit(e.currentTarget.value);
      e.currentTarget.value = '';
    }
  }
  return <div>
    <input type="text" onKeyPress={addTodoHandler}/>
  </div>
};
