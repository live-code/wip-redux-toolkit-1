import React, {  useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addNews, toggleNews } from './store/news.store';
import { getNews } from './store/news.selector';

export const HomePage: React.FC = () => {
  const [showPublished, setShowPublished] = useState<string>('all');
  const newsList = useSelector(getNews(showPublished))
  const dispatch = useDispatch();

  function onChange(e: React.KeyboardEvent<HTMLInputElement>) {
    if(e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value))
      e.currentTarget.value = ''
    }
  }

  function visibilityHandler(e: React.ChangeEvent<HTMLSelectElement>) {
    setShowPublished(e.currentTarget.value)
  }

  return <div>
    <input type="text" onKeyPress={onChange} />

    <select onChange={visibilityHandler}>
      <option value="all">Show all</option>
      <option value="published">Published</option>
      <option value="unpublished">Unublished</option>
    </select>

    {
      newsList.map(news => {
        return (
          <li className="list-group-item" key={news.id}>
            {news.title}

            <div
              className="badge badge-dark ml-3"
              onClick={() => dispatch(toggleNews(news.id))}
            >
              {news.published ? 'PUBLISHED' : 'UNPUBLISHED'}
            </div>
          </li>
        )
      })
    }
  </div>
};
