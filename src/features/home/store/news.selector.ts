import { RootState } from '../../../App';

export const getNews = (filterState: string) => (state: RootState) => {
  switch(filterState) {
    case 'published': return state.news.filter(n => n.published);
    case 'unpublished': return state.news.filter(n => !n.published);
    default: return state.news
  }
}
