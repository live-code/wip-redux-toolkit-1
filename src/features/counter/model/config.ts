export interface Config {
  pallet: number; // 12, 6, 4
  color: string;
  material: 'wood' | 'metal'
}
