import { createReducer } from '@reduxjs/toolkit';
import { decrement, increment, reset } from './counter.actions';

export const counterReducer = createReducer(0, {
  [increment.type]: (state, { payload = 1 }) => {
    return state + payload;
  },
  [decrement.type]: (state, action) => {
    if (state > action.payload) {
      return state - action.payload
    }
    return 0;
  },
  [reset.type]: () => 0
})
