import { createAction } from '@reduxjs/toolkit';

export const decrement = createAction<number>('decrement')
export const increment = createAction<number | undefined>('increment')
export const reset = createAction('reset')
