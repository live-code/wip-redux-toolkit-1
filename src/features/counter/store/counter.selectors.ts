import { RootState } from '../../../App';

export const getCounter = (state: RootState) => state.shipping.value;
export const getCounterPallet = (state: RootState) => {
  return Math.ceil(state.shipping.value / state.shipping.config.pallet)
}
