import { createReducer } from '@reduxjs/toolkit';
import { Config } from '../model/config';
import { setConfig } from './config.actions';

const INITIAL_STATE: Config = { pallet: 12, color: 'red', material: 'metal' };

export const configReducer = createReducer<Config>(INITIAL_STATE, {
  [setConfig.type]: (state, action) => {
    return {
      ...state,
      ...action.payload
    };
  },
})
