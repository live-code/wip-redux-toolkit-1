import React  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { decrement, increment, reset } from './store/counter.actions';
import { getCounter, getCounterPallet } from './store/counter.selectors';
import { setConfig } from './store/config.actions';
import { RootState } from '../../App';

export const CounterPage: React.FC = () => {
  const counter = useSelector(getCounter);
  const counterPallet = useSelector(getCounterPallet)
  const backgroundColor = useSelector((state: RootState) => state.shipping.config.color)
  const dispatch = useDispatch();

  return <div>
    <h1 style={{ backgroundColor }}>Counter: {counter}</h1>
    <h2>Pallet: {counterPallet}</h2>

    <button onClick={() => dispatch(decrement(10))}>-</button>
    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(reset())}>reset</button>
    <hr/>
    <button onClick={() => dispatch(setConfig({ pallet: 6}))}>Set Pallet to 6</button>
    <button onClick={() => dispatch(setConfig({ pallet: 12}))}>Set Pallet to 12</button>
    <button onClick={() => dispatch(setConfig({ color: 'green'}))}>Set green</button>
    <button onClick={() => dispatch(setConfig({ material: 'wood'}))}>Change to wood</button>
  </div>
};

