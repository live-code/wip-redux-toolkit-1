import React  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { addUser, deleteUser, setAsAdmin } from './store/users.store';

export const UsersPage: React.FC = () => {
  const dispatch = useDispatch();
  const users = useSelector((state: RootState) => state.users)

  function addUserHandler() {
    const id =  Date.now();
    dispatch(addUser({
      id,
      name: 'user ' + id,
      isAdmin: false
    }))
  }

  return <div>
    <button onClick={addUserHandler}>add</button>
    <ul>
      {
        users.map(u => {
          return (
            <li key={u.id} style={{ color: u.isAdmin ? 'green' : ''}}>
              {u.name}
              <i className="fa fa-trash" onClick={() => dispatch(deleteUser(u.id))}/>
              <button onClick={() => dispatch(setAsAdmin(u))}>Set Admin</button>
            </li>
          )
        })
      }
    </ul>
  </div>
};
