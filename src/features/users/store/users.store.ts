import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { User } from '../model/user';
import { setError } from '../../../core/store/http-error.store';

export const usersStore = createSlice({
  name: 'users',
  initialState: [] as User[],
  reducers: {
    addUser(state, action: PayloadAction<User>) {
      state.push(action.payload);
      // return [...state, action.payload]
    },
    deleteUser(state, action: PayloadAction<number>) {
      const index = state.findIndex(u => u.id === action.payload);
      state.splice(index, 1);
    },
    setAsAdmin(state, action: PayloadAction<User>) {
      const user = state.find(u => u.id === action.payload.id);
      if(user) {
        user.isAdmin = !user.isAdmin;
      }
    },
  },

})

export const { addUser, deleteUser, setAsAdmin } = usersStore.actions;
