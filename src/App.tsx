import React from 'react';
import './App.css';
import { Navbar } from './core/components/Navbar';
import { combineReducers, configureStore, ThunkAction, Action, getDefaultMiddleware } from '@reduxjs/toolkit';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { HomePage } from './features/home/HomePage';
import { CounterPage } from './features/counter/CounterPage';
import { CatalogPage } from './features/catalog/CatalogPage';
import { UsersPage } from './features/users/UsersPage';
import { Provider } from 'react-redux';
import { shippingReducer } from './features/counter/store';
import { usersStore } from './features/users/store/users.store';
import { newsStore } from './features/home/store/news.store';
import { productsStore } from './features/catalog/store/products.store';
import { httpErrorStore } from './core/store/http-error.store';

const rootReducer = combineReducers({
  todos: () => [],
  catalog: productsStore.reducer,
  users: usersStore.reducer,
  shipping: shippingReducer,
  news: newsStore.reducer,
  httpError: httpErrorStore.reducer
})
export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>;
export type AppDispatch = typeof store.dispatch;
export const store = configureStore({
  reducer: rootReducer,
 // devTools: process.env.NODE_ENV !== 'production'
})


function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route path="/" exact={true}>
            <HomePage />
          </Route>
          <Route path="/counter">
            <CounterPage />
          </Route>
          <Route path="/users">
            <UsersPage />
          </Route>
          <Route path="/catalog">
            <CatalogPage />
          </Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
