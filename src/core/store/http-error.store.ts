import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export const httpErrorStore = createSlice({
  name: 'httpError',
  initialState: false,
  reducers: {
    setError(state, action: PayloadAction<boolean>) {
      return action.payload
    },
  },
  extraReducers: {
    ['products/add/rejected']: () =>  true,
  }
})

export const { setError } = httpErrorStore.actions;
