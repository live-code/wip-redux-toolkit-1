import { Middleware } from "@reduxjs/toolkit";
import { RootState } from '../../App';
// DA SISTEMARE
export const logger: Middleware<{ }, RootState> = (store) => {
  return next => {
    return action => {
      try {
        console.log('next', next)
        console.log('current state', store.getState());
        console.log('dispatching', action);
        let result = next(action)
        console.log('next state', store.getState())
        return result
      } catch(e) {
        console.log(e)
      }

    }
  }
}

//App.tsx
// Configure Store
/*
export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: [...getDefaultMiddleware(), logger]
});*/
